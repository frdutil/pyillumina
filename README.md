# pyillumina

Python implementation of illumina, a light pollution model developed by Pr. Martin Aubé.
The project description can be found here: https://w1.cegepsherbrooke.qc.ca/~aubema/index.php/Prof/IllumEn

# Original code
The original code is in fortran 77, and can be found here: https://bitbucket.org/aubema/illumina?fbclid=IwAR2xgn58yxTqcuAvAogb6iq2EKzz1s_b2OnNArqkWtyr-5GB7WVnGCaep6w
With it's documentation: https://w1.cegepsherbrooke.qc.ca/~aubema/index.php/Prof/IlluminaGuide2016?fbclid=IwAR0gJeIDvD3s_b7Jf9icpwp4lKDH3Pk6wVxAbc3rosW5KyjqU-mSaB4zUaQ

# Installation

TODO

# How to